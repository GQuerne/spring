package fr.ajc.spring.querne.tp;

import java.util.Random;
import java.util.UUID;

import org.springframework.http.MediaType;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import fr.ajc.spring.querne.tp.model.Article;

public class UtilTest {
	
	private static final String API_ROOT = "http://localhost:8081/api/articles";
	
	String createArticleAsUri(Article article) {
		System.out.println("createArticleAsUri called");
		Response response =
				RestAssured.given().contentType(MediaType.APPLICATION_JSON_VALUE).body(article).post(API_ROOT+"/create"+article.getName());
		return API_ROOT + "/" + response.jsonPath().get("id");
	}
	
	Article createRandomArticle() {
		System.out.println("createRandomArticle called");
		Random Random = new Random();
		long id = Random.nextInt();
		
		UUID nameR = UUID.randomUUID();
		System.out.println("name :"+String.valueOf(nameR));
		String name = String.valueOf(nameR);
		
		float price = Random.nextFloat();
		
		Article article = new Article(id, name, price);
		return article;
	}

}
