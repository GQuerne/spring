package fr.ajc.spring.querne.tp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import fr.ajc.spring.querne.tp.model.Article;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TpApplicationTests {

	private static final String API_ROOT = "http://localhost:8081/api/articles";
	UtilTest util = new UtilTest();

	@Test
	public void contextLoads() {
	}

	@Test
	public void whenGetAllArticles_thenOK() {
		Response response = RestAssured.get(API_ROOT+"/all");
		System.out.println(response.body().asString());
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
	}

	@Test
	public void whenGetArticleByName_thenOK() {
		System.out.println("whenGetArticleByName_thenOK, call createRandomArticle");
		Article article = util.createRandomArticle();
		util.createArticleAsUri(article);
		System.out.println(article);
		Response response = RestAssured.get(
				API_ROOT+"/one?name="+article.getName());
		System.out.println(response.body().asString());
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
	}
}
