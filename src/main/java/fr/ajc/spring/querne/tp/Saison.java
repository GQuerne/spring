package fr.ajc.spring.querne.tp;

public class Saison {
	private String name;
	private Boolean isCurrent;
	
	public Saison() {
	}

	public Saison(String name, Boolean isCurrent) {
		this.name = name;
		this.isCurrent = isCurrent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsCurrent() {
		return isCurrent;
	}

	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	@Override
	public String toString() {
		return "Saison [name=" + name + ", isCurrent=" + isCurrent + "]";
	}
	
}
