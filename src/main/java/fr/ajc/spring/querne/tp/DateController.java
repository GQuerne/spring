package fr.ajc.spring.querne.tp;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DateController {
	
	@Value("${spring.application.name}")
	String appName;
	
	LocalDateTime today;
	int todayDOY;
	String saison;
	
	@GetMapping("/date")
	public String datePage(Model model) {
		model.addAttribute("appName", appName);
		
		today = LocalDateTime.now();
		todayDOY = LocalDateTime.now().getDayOfYear();
		model.addAttribute("localDateTime", today);
		
		if (todayDOY >= 80 && todayDOY < 173) {
			model.addAttribute("saison", "spring");
		} else if(todayDOY >= 173 && todayDOY < 266) {
			model.addAttribute("saison", "summer");
		} else if(todayDOY >= 266 && todayDOY < 354) {
			model.addAttribute("saison", "fall");
		} else {
			model.addAttribute("saison", "winter");
		}
		return "date";
	}
}