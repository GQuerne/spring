package fr.ajc.spring.querne.tp;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.ajc.spring.querne.tp.model.Article;
import fr.ajc.spring.querne.tp.repo.ArticleRepository;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

	@Autowired
	private ArticleRepository articleRepository;

	@GetMapping("/all")
	public Iterable<Article> findAll() {
		return articleRepository.findAll();
	}

	@GetMapping("/one")
	public List<Article> findByName (@RequestParam String name) {
		return articleRepository.findByName(name);
	}

	@PostMapping("/create")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Article create(@Valid@ModelAttribute("article") Article article, BindingResult result, Model model) {
		if (result.hasErrors()) {
			System.out.println("Invalid article");
			return null;
		} else {
			System.out.println("Name : " +article.getName());
			System.out.println("Price : "+article.getPrice());
			return articleRepository.save(article);
		}
	}

	@DeleteMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(Long id) {
		articleRepository.deleteById(id);
	}

}
