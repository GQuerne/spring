package fr.ajc.spring.querne.tp;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SaisonsController {
	
	@Value("${spring.application.name}")
	String appName;
	
	int today;
	
	Saison spring;
	Saison summer;
	Saison fall;
	Saison winter;
	
	List<Saison> seasons;
	
	@GetMapping("/saisons")
	public String saisonsPage(Model model) {
		model.addAttribute("appName", appName);
		
		today = LocalDateTime.now().getDayOfYear();
		model.addAttribute("localDateTime", today);
		
		
		spring = new Saison("spring", false);
		summer = new Saison("summer", false);
		fall = new Saison("fall", false);
		winter = new Saison("winter", false);
		
		seasons = new ArrayList<Saison>();
		
		if (today >= 80 && today < 173) {
			spring.setIsCurrent(true);
		} else if(today >= 173 && today < 266) {
			summer.setIsCurrent(true);
		} else if(today >= 266 && today < 354) {
			fall.setIsCurrent(true);
		} else {
			winter.setIsCurrent(true);
		}
		
		seasons.add(spring);
		seasons.add(summer);
		seasons.add(fall);
		seasons.add(winter);
		
		model.addAttribute("seasons", seasons);
		return "saisons";
	}
}
