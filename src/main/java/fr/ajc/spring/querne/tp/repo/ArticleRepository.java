package fr.ajc.spring.querne.tp.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.ajc.spring.querne.tp.model.Article;

public interface ArticleRepository extends CrudRepository<Article, Long> {
	List<Article> findByName(String name);
}
